package benchmark

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

type Example struct {
	Name   string
	Size   int
	Prompt []byte
}

func (e Example) String() string {
	return fmt.Sprintf("%s (%d bytes)", e.Name, e.Size)
}

func LoadExamples(path string) ([]Example, error) {
	examples := []Example{}

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return examples, err
	}

	for _, file := range files {
		if !file.IsDir() {
			f, err := os.Open(filepath.Join(path, file.Name()))
			defer f.Close()
			if err != nil {
				return examples, err
			}

			content, err := ioutil.ReadAll(f)
			if err != nil {
				return examples, err
			}

			example := Example{
				Name:   file.Name(),
				Size:   len(content),
				Prompt: content,
			}

			examples = append(examples, example)
		}
	}

	return examples, nil
}
