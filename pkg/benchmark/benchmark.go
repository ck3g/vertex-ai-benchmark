package benchmark

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/ck3g/vertex-ai-benchmark/pkg/vertexai"
)

type BenchmarkParams struct {
	Requests        int
	Examples        []Example
	Models          []string
	CandidatesCount int
	FakeRun         bool
	Verbose         bool
	StatusCodes     bool
}

type BenchmarkResponse struct {
	Status     string
	StatusCode int
	ExecTime   time.Duration
}

type (
	CandidateResults map[int][]BenchmarkResponse
	ModelResults     map[string]CandidateResults
	BenchmarkResults map[string]ModelResults
)

func Run(client vertexai.Client, bp BenchmarkParams, verboseChan chan []string, resultChan chan *BenchmarkResults) {
	defer close(verboseChan)

	var (
		prevExample = ""
		prevModel   = ""
	)

	results := BenchmarkResults{}

	for _, example := range bp.Examples {
		modelResults := ModelResults{}
		for _, model := range bp.Models {
			candidateResults := CandidateResults{}

			for i := 0; i < bp.Requests; i++ {
				exampleTitle := example.String()
				if prevExample != example.String() {
					prevExample = example.String()
				} else {
					exampleTitle = ""
				}

				modelTitle := model
				if prevModel != model {
					prevModel = model
				} else {
					modelTitle = ""
				}

				modelData := []string{exampleTitle, modelTitle}

				for i := 1; i <= bp.CandidatesCount; i++ {
					rp := vertexai.NewRequestParams(model, i, string(example.Prompt), bp.FakeRun)

					resp, err := BenchmarkRequest(client, rp)
					if err != nil {
						log.Println(err)
					}

					candidateResults[i] = append(candidateResults[i], resp)

					execTime := fmt.Sprintf("%d", resp.ExecTime.Milliseconds())
					if bp.Verbose && bp.StatusCodes {
						execTime = fmt.Sprintf("%d (%d)", resp.ExecTime.Milliseconds(), resp.StatusCode)
					}
					modelData = append(modelData, execTime)
				}

				verboseChan <- modelData
			}

			verboseChan <- []string{}

			modelResults[model] = candidateResults
		}

		results[example.String()] = modelResults
	}

	resultChan <- &results
}

func BenchmarkRequest(client vertexai.Client, rp vertexai.RequestParams) (BenchmarkResponse, error) {
	startTime := time.Now()
	res, err := client.Predict(rp)
	endTime := time.Now()
	executionTime := endTime.Sub(startTime)

	if err != nil {
		return BenchmarkResponse{ExecTime: executionTime}, err
	}

	return BenchmarkResponse{
		Status:     res.Status,
		StatusCode: res.StatusCode,
		ExecTime:   executionTime,
	}, err
}

func CalculateMean(values []BenchmarkResponse) time.Duration {
	var sum time.Duration
	for _, value := range values {
		sum += value.ExecTime
	}

	return sum / time.Duration(len(values))
}

func CalculateMedian(values []BenchmarkResponse) time.Duration {
	if len(values) == 0 {
		return 0
	}

	if len(values)%2 == 0 {
		return (values[len(values)/2-1].ExecTime + values[len(values)/2].ExecTime) / 2
	}

	return values[len(values)/2].ExecTime
}
