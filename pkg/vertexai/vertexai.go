package vertexai

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"math/big"
	"net/http"
	"time"
)

type Client struct {
	APIEndpoint string
	ProjectID   string
	AccessToken string
}

type RequestParams struct {
	ModelID         string
	Prompt          string
	CandidateCount  int
	MaxOutputTokens int
	Temperature     float32
	FakeRun         bool
}

type RequestResponse struct {
	Status     string
	StatusCode int
}

type RequestPayload struct {
	Instances  []RequestPayloadInstance `json:"instances"`
	Parameters RequestPayloadParameters `json:"parameters"`
}

type RequestPayloadInstance struct {
	Prefix string `json:"prefix"`
}

type RequestPayloadParameters struct {
	CandidateCount  int     `json:"candidateCount"`
	MaxOutputTokens int     `json:"maxOutputTokens"`
	Temperature     float32 `json:"temperature"`
}

func NewClient(apiEndpoint, projectID, accessToken string) Client {
	return Client{
		APIEndpoint: apiEndpoint,
		ProjectID:   projectID,
		AccessToken: accessToken,
	}
}

func NewRequestParams(modelID string, candidateCount int, prompt string, fakeRun bool) RequestParams {
	maxTokens := 1024
	if modelID == "code-gecko@001" {
		maxTokens = 64
	}

	return RequestParams{
		Prompt:          prompt,
		ModelID:         modelID,
		CandidateCount:  candidateCount,
		MaxOutputTokens: maxTokens,
		Temperature:     0.2,
		FakeRun:         fakeRun,
	}
}

func (c *Client) NewRequestPayload(rp RequestParams) RequestPayload {
	return RequestPayload{
		Instances: []RequestPayloadInstance{
			{
				Prefix: rp.Prompt,
			},
		},
		Parameters: RequestPayloadParameters{
			CandidateCount:  rp.CandidateCount,
			MaxOutputTokens: rp.MaxOutputTokens,
			Temperature:     rp.Temperature,
		},
	}
}

func (c *Client) Predict(rp RequestParams) (*RequestResponse, error) {
	if rp.FakeRun {
		time.Sleep(generateRandomLatency())
		return &RequestResponse{Status: "200 OK", StatusCode: 200}, nil
	}

	payloadBytes, err := json.Marshal(c.NewRequestPayload(rp))
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf(
		"https://%s/v1/projects/%s/locations/us-central1/publishers/google/models/%s:predict",
		c.APIEndpoint, c.ProjectID, rp.ModelID,
	)

	req, err := http.NewRequest("POST", apiURL, bytes.NewBuffer(payloadBytes))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+c.AccessToken)
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return &RequestResponse{Status: resp.Status, StatusCode: resp.StatusCode}, nil
}

func generateRandomLatency() time.Duration {
	var randLatency *big.Int
	randLatency, err := rand.Int(rand.Reader, big.NewInt(101))
	if err != nil {
		randLatency = big.NewInt(101)
	}
	randLantency := randLatency.Add(randLatency, big.NewInt(100))
	return time.Duration(randLantency.Int64()) * time.Millisecond
}
