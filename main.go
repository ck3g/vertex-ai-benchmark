package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
	"gitlab.com/ck3g/vertex-ai-benchmark/pkg/benchmark"
	"gitlab.com/ck3g/vertex-ai-benchmark/pkg/vertexai"
)

const (
	exampleDefaultWidth        = 25
	modelWidth                 = 20
	durationWidth              = 18
	defaultCandidatesCount int = 4
)

var (
	models = []string{"code-gecko@001", "code-bison@001", "code-bison@latest"}

	blankRow  = []string{"", ""}
	header    = []string{"Example", "Model"}
	colWidths = []int{exampleDefaultWidth, modelWidth}
)

func main() {
	fmt.Printf("Vertex AI Benchmark\n\n")

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	modelNames := flag.String("models", strings.Join(models, ","), "Comma-separated list of model names")
	candidates := flag.Int("candidates", defaultCandidatesCount, "Number of candidates count. Should be a number between 1 and 4")
	accessToken := flag.String("access-token", "", "Access token for Vertex AI API")
	apiEndpoint := flag.String("api-endpoint", os.Getenv("API_ENDPOINT"), "API endpoint for Vertex AI API")
	projectID := flag.String("project-id", os.Getenv("PROJECT_ID"), "Project ID for Vertex AI API")
	requests := flag.Int("n", 5, "Number of requests to run")
	examplesPath := flag.String("examples-path", "./examples", "Path to examples directory")
	verbose := flag.Bool("verbose", false, "Print detailed information about each request")
	statusCodes := flag.Bool("status-codes", false, "Show status code for each request in verbose mode")
	fakeRun := flag.Bool("fake-run", false, "Run without sending requests to Vertex AI API")
	flag.Parse()

	models := strings.Split(*modelNames, ",")

	client := vertexai.NewClient(*apiEndpoint, *projectID, *accessToken)

	fmt.Printf("Loading examples...\n\n")
	examples, err := benchmark.LoadExamples(*examplesPath)
	if err != nil {
		log.Fatalln(err)
	}
	if len(examples) == 0 {
		fmt.Println("No examples found. Please add examples to the examples directory and try again.")
		os.Exit(0)
	}

	colWidths[0] = exampleWidth(examples)

	// Process candidates count
	candidatesCount := *candidates
	if candidatesCount < 1 || candidatesCount > 4 {
		candidatesCount = defaultCandidatesCount
	}
	for i := 1; i <= candidatesCount; i++ {
		blankRow = append(blankRow, "")
		header = append(header, fmt.Sprintf("Candidate %d (ms)", i))
		statusSpace := 0
		if *verbose && *statusCodes {
			statusSpace = 6
		}
		colWidths = append(colWidths, durationWidth+statusSpace)
	}

	fmt.Printf("Benchmarking...\n\n")

	fmt.Println("Examples:", len(examples))
	fmt.Println("Models:", strings.Join(models, ", "))
	fmt.Println("Candidates count:", candidatesCount)
	fmt.Println("Requests:", *requests)
	if *fakeRun {
		fmt.Println("Fake run. No requests will be sent to Vertex AI API.")
	}
	fmt.Println()

	bp := benchmark.BenchmarkParams{
		Requests:        *requests,
		Examples:        examples,
		Models:          models,
		CandidatesCount: candidatesCount,
		FakeRun:         *fakeRun,
		Verbose:         *verbose,
		StatusCodes:     *statusCodes,
	}

	if bp.Verbose {
		printRow(header, colWidths)
		fmt.Println()
	} else {
		fmt.Print("Benchmarking (be patient)")
	}

	verboseChan := make(chan []string)
	resultChan := make(chan *benchmark.BenchmarkResults, 1)

	go benchmark.Run(client, bp, verboseChan, resultChan)

	for out := range verboseChan {
		if bp.Verbose {
			if len(out) == 0 {
				fmt.Println()
			} else {
				printRow(out, colWidths)
			}
		} else {
			fmt.Print(".")
		}
	}

	res := <-resultChan
	close(resultChan)

	if !bp.Verbose {
		fmt.Println("done")
	}

	numberOfRequests := 0
	numberOfNon200Requests := 0
	modelLatency := map[string][]benchmark.BenchmarkResponse{}
	for _, example := range *res {
		for model, modelResults := range example {
			modelLatency[model] = []benchmark.BenchmarkResponse{}

			for _, candidateResults := range modelResults {
				numberOfRequests += len(candidateResults)
				modelLatency[model] = append(modelLatency[model], candidateResults...)

				for _, result := range candidateResults {
					if result.StatusCode < 200 || result.StatusCode >= 300 {
						numberOfNon200Requests++
					}
				}
			}
		}
	}

	fmt.Println()
	printRow([]string{"Complete requests:", fmt.Sprintf("%d", numberOfRequests)}, []int{20, 5})
	printRow([]string{"Failed requests:", fmt.Sprintf("%d", numberOfNon200Requests)}, []int{20, 5})
	fmt.Println()

	fmt.Printf("Connection times (ms)\n\n")
	width := 8
	summaryWidths := []int{modelWidth, width, width, width, width}
	printRow([]string{"Model", "min", "mean", "median", "max"}, summaryWidths)

	for name, modelResult := range modelLatency {
		min := modelResult[0].ExecTime
		max := modelResult[0].ExecTime
		for _, result := range modelResult {
			if result.ExecTime < min {
				min = result.ExecTime
			}
			if result.ExecTime > max {
				max = result.ExecTime
			}
		}

		mean := benchmark.CalculateMean(modelResult)
		median := benchmark.CalculateMedian(modelResult)

		minStr := fmt.Sprintf("%d", min.Milliseconds())
		meanStr := fmt.Sprintf("%d", mean.Milliseconds())
		medianStr := fmt.Sprintf("%d", median.Milliseconds())
		maxStr := fmt.Sprintf("%d", max.Milliseconds())

		printRow([]string{name, minStr, meanStr, medianStr, maxStr}, summaryWidths)
	}
}

func printRow(row []string, colWidths []int) {
	for i, cell := range row {
		fmt.Printf("%-*s", colWidths[i], cell)
	}
	fmt.Println()
}

func exampleWidth(examples []benchmark.Example) int {
	width := exampleDefaultWidth
	for _, example := range examples {
		if len(example.String()) > width {
			width = len(example.String()) + 2
		}
	}

	return width
}
