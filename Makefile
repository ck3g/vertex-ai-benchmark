
build:
	@go build -o bin/vbm

run: build
	@./bin/vbm -access-token=$(shell gcloud auth print-access-token) -n 2 -candidates 2 --fake-run --models=code-bison@001,code-gecko@001 --verbose

test:
	@go test -v ./...