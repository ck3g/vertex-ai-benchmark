# Vertex AI Benchmark

Vertex AI Benchmark is a tool to test Vertex AI Codey APIs to compare with different parameters.

## How to use

1. Provide example prompts as list of files in `examples` directory. Where the filename is an example name and the content is the prompt.
1. `cp .env.example .env` and fill in the environment variables.
1. Ensure you're authenticated with the `gcloud` CLI by running `gcloud auth application-default login`
1. Run `./bin/vbm -access-token=$(gcloud auth print-access-token)`

## Example output

```
Vertex AI Benchmark

Loading examples...

Benchmarking...

Examples: 5
Models: code-gecko@latest, code-gecko@001, code-bison@001, code-bison@latest
Candidates count: 4
Requests: 5

Example                           Model               Candidate 1 (ms)  Candidate 2 (ms)  Candidate 3 (ms)  Candidate 4 (ms)

golang-bubble-sort (70 bytes)     code-gecko@latest   807               189               633               177
                                                      641               188               526               202
                                                      512               143               171               143
                                                      147               142               146               141
                                                      140               137               139               142

                                  code-gecko@001      305               357               404               387
                                                      332               153               153               152
                                                      148               149               141               143
                                                      141               140               141               148
                                                      143               142               142               148

                                  code-bison@001      1509              1347              1606              1330
                                                      1428              1512              1444              1325
                                                      1560              1537              1239              1254
                                                      1433              1276              1458              1333
                                                      1415              1534              1341              1330

                                  code-bison@latest   2900              2936              2890              2877
                                                      3368              2742              150               149
                                                      152               149               141               146
                                                      143               143               138               139
                                                      140               144               138               138

golang-fizzbuzz (423 bytes)       code-gecko@latest   150               156               157               170
                                                      154               148               146               153
                                                      163               164               137               142
                                                      144               136               165               141
                                                      146               139               138               137

                                  code-gecko@001      149               154               152               147
                                                      150               144               148               148
                                                      152               151               143               439
                                                      344               380               370               144
                                                      300               475               315               698

                                  code-bison@001      170               153               152               150
                                                      155               153               150               154
                                                      151               153               142               143
                                                      143               141               147               139
                                                      137               149               141               138

                                  code-bison@latest   137               141               142               141
                                                      142               142               329               138
                                                      145               139               173               141
                                                      2439              138               2287              2337
                                                      2268              2298              2335              2303

javascript-api-call (294 bytes)   code-gecko@latest   147               144               142               138
                                                      147               158               143               141
                                                      142               144               146               139
                                                      143               134               138               140
                                                      139               140               142               140

                                  code-gecko@001      142               144               147               140
                                                      148               144               144               140
                                                      145               137               154               143
                                                      143               135               143               142
                                                      148               145               138               143

                                  code-bison@001      2117              141               1842              143
                                                      1821              2309              1792              1584
                                                      2188              3175              1553              144
                                                      142               140               150               144
                                                      142               141               145               141

                                  code-bison@latest   151               152               148               149
                                                      150               140               139               138
                                                      140               141               139               136
                                                      336               139               141               170
                                                      140               141               636               137

javascript-array-uniq (64 bytes)  code-gecko@latest   140               136               138               143
                                                      140               141               135               141
                                                      145               138               138               139
                                                      146               147               141               139
                                                      142               142               143               141

                                  code-gecko@001      143               146               161               144
                                                      147               146               143               141
                                                      145               144               142               146
                                                      141               141               144               140
                                                      136               138               143               144

                                  code-bison@001      139               148               149               146
                                                      144               146               140               144
                                                      146               142               176               144
                                                      145               152               144               142
                                                      148               143               139               145

                                  code-bison@latest   136               145               142               136
                                                      145               137               136               139
                                                      146               137               132               141
                                                      166               139               137               141
                                                      146               137               139               141

ruby-api-call (278 bytes)         code-gecko@latest   134               142               139               137
                                                      144               144               141               138
                                                      141               143               143               139
                                                      141               142               145               141
                                                      143               141               147               155

                                  code-gecko@001      453               368               144               142
                                                      145               138               142               143
                                                      141               146               137               146
                                                      142               143               145               162
                                                      143               143               142               141

                                  code-bison@001      141               139               140               143
                                                      146               142               143               142
                                                      143               140               2271              2139
                                                      2247              2409              2230              2335
                                                      2049              2251              2098              2301

                                  code-bison@latest   2833              2916              2896              2123
                                                      2444              2436              153               151
                                                      156               156               146               151
                                                      141               140               135               137
                                                      150               134               137               143


Complete requests:  400
Failed requests:    327

Connection times (ms)

Model               min     mean    median  max
code-bison@latest   134     884     1515    2916
code-gecko@latest   134     142     140     155
code-gecko@001      137     170     143     453
code-bison@001      139     1187    1120    2409
```

